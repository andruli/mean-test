const mongoose = require('mongoose');

// Define Schema
const UseCaseSchema = mongoose.Schema({
    title: String,
    body: String,
    milestones: [{
        id: String,
        name: String,
        name_de: String,
        name_en: String,
        start_date: Date,
        end_date: Date,
        usecase: Number
    }]
});

// Define Model
const UseCases =  mongoose.model('usecases', UseCaseSchema);

module.exports = { UseCases: UseCases };