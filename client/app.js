// Load vis
const vis = require('vis');
// Load angular
const angular = require('angular');


// Create the main module and name it motiusApp
const motiusApp = angular.module('motiusApp', [
  require('angular-route'),
  require('angular-material'),
  require('angular-material-icons')
]);

// configure our routes
motiusApp.config(function($routeProvider) {
  $routeProvider
    // route for the home page
    .when('/', {
      templateUrl : '/dist/templates/dash.html',
      controller  : 'dashboardController'
    })
    .when('/add', {
      templateUrl : '/dist/templates/add.html',
      controller  : 'addController'
    })
});

motiusApp.controller('toolbarController', function($scope, $location) {
  $scope.onUsecasesClick = () => $location.path('/');
  $scope.onAddUsecaseClick = () => $location.path('/add');
});

motiusApp.controller('milestonesController', function($scope, $mdDialog, usecase) {
  $scope.usecase = usecase;
  $scope.onCloseClick = () => $mdDialog.cancel();
  // Render milestones
  setTimeout(() => {
    console.log('Executing this');
    new vis.Timeline(
      document.getElementById('milestones-timeline'),
      new vis.DataSet(
        usecase.milestones.map(m => ({
          id: m.id,
          start: m.start_date,
          end: m.end_date,
          content: m.name
        }))
      ),
      {}
    );
  }, 100);
});

motiusApp.controller('dashboardController', function($scope, $http, $mdDialog) {
  $scope.loading = true;
  $scope.onViewMilestonesClick = usecase => {
    $mdDialog.show({
      controller: 'milestonesController',
      templateUrl: "/dist/templates/milestones.html",
      parent: angular.element(document.body),
      clickOutsideToClose:true,
      locals: {
        usecase: usecase
      }
    });
  };
  // Fetch usecases from the server
  $http.get('/api/usecases')
    .then(
      response => {
        $scope.usecases = response.data;
      },
      error => {
        console.error(error);
      })
    .then(() => { $scope.loading = false });
});

motiusApp.controller('addController', function($scope, $http, $location) {
  $scope.submiting = false;
  $scope.usecase = {
    title: '',
    body: ''
  };
  $scope.onSubmitFormClick = () => {
    if (!$scope.usecase.title) return;
    $scope.submiting = true;
    $http.post('/api/usecases', $scope.usecase)
      .then(
        response => $location.path('/'),
        error => {
          console.error(error);
        })
      .then(() => { $scope.loading = false });
  };
});