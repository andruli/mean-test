### Description

This is a MEAN stack project build as a proof of concept for [Motius](https://www.motius.de)

### Build & Run

#### Requirements
- [nvm](https://github.com/creationix/nvm)
- [mongodb](https://docs.mongodb.com/)
- `Usecasedata.json` DB dump

#### DB
Ensure you've properly set up `mongodb` and restore the *DB dump* from the `Usecasedata.json` file. The project assumes you've `mongodb` running locally in the same machine.

```
mongoimport Usecasedata.json -d test -c usecases --jsonArray
```
#### Frontend assets

The project uses `nvm` to easily manage and use the right version of node.

1- Run `nvm use` to use the right version of node. If you get an error message saying `N/A: version "vx.y.z" is not yet installed` run `nvm install x.y.z` .

2- Install required packages, run `npm install` at the root of this repo.

3- Build frontend assets, run `gulp` at the root of this repo.

#### Run server
```
node app.js
```

And you're all set, you should be able to access the webapp at `localhost:3000`.