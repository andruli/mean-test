// External Libs
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// Use Cases Model
const UseCases = require('./db/usecases.js').UseCases;

app();

function app() {
  connectToDB();
  startExpress();
};

function connectToDB() {
  // Connect mongoose with local mongodb instance
  mongoose.connect('mongodb://localhost/test');
  mongoose.connection
    .on('error', function() {
      console.error('X Couldn\'t connect to test db');
      process.exit(2)
    })
    .once('open', function() {
      console.log('> Connected to DB');
    });
}

function startExpress(conn) {
  const app = express();

  // Set body parser as middleware
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  // Serve static files from the public folder
  app.use(express.static('public'));

  // API
  app.get('/api/usecases', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    UseCases.find({}, null, { sort: { _id: -1 }}, (error, result) => {
      if (error) {
        res.status(500).send(error);
      } else {
        res.send(JSON.stringify(result));
      }
    });
  });

  app.get('/api/usecases/:usecaseId', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    UseCases.findById(req.params['usecaseId'], (error, result) => {
      if (error) {
        res.status(500).send(error);
      } else {
        res.send(JSON.stringify(result));
      }
    });
  });

  // respond with "hello world" when a GET request is made to the homepage
  app.post('/api/usecases', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    new UseCases(req.body).save((error, result) => {
      if (error) {
        res.status(500).send(error);
      } else {
        res.send(JSON.stringify(result));
      }
    });
  });

  // Define Routes
  app.get('*', function(req, res) {
    res.sendFile('./public/index.html', { root: __dirname });
  });

  // Start server
  app.listen(3000, function () {
    console.log('> Server started on localhost:3000');
  });
}

