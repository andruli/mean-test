const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

// Where to build our assets
const buildDir = 'public/dist';

gulp.task('default', ['build']);

gulp.task('build', ['scripts', 'styles', 'templates']);

/****************************************************
 * JavaScript tasks                                 *
 ****************************************************/
gulp.task('scripts', function () {
  return gulp.src('client/**/*.js')
  .pipe($.babel())
  .pipe($.browserify({
    insertGlobals : true,
    debug : true
  }))
  .pipe(gulp.dest('public/dist'));
});

/****************************************************
 * Styles tasks                                     *
 ****************************************************/
gulp.task('styles', function () {
  gulp.src('client/style/main.scss')
    .pipe($.sass({
      outputStyle: 'expanded',
      precision: 6,
      includePaths: ['node_modules']
    }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(buildDir))
});

/****************************************************
 * Views tasks                                      *
 ****************************************************/
gulp.task('templates', function() {
  return gulp.src('client/templates/**/*')
    .pipe(gulp.dest(buildDir + '/templates'));
});